import type { PlasmoGetStyle } from "plasmo"
import rootCssText from "data-text:~style-tw-base.css"
import rootUtilsCssText from "data-text:~style-tw-utils.css"
export const getGetStyle = (cssText = '') => {
    const retFx: PlasmoGetStyle = () => {
        // initUno()
        const customCss = cssText
        ///* rootCssText // moved to preflights */}
        const style = document.createElement("style")
        style.textContent = `
${rootCssText}
${rootUtilsCssText}
${customCss}
`
        // console.log({ customCss, cssText })

        return style
    }
    // console.log(retFx, cssText)
    return retFx
}