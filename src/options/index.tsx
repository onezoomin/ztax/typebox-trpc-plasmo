import { sendToBackground } from '@plasmohq/messaging'
import { Value } from '@sinclair/typebox/value'
import { useState } from 'react'
import { AtomDuck, castAbleImperfectAtom } from '~/model/AppLogType'
import { TInput, addInput, addOutput } from '~/model/IoType'
import { Disclosure } from '@headlessui/react'

import "~style-tw-base.css"
import "~style-tw-utils.css"
import "./style.css"

// import rootCss from "data-text:~style.css"
// import * as rootCssMod from "~style.css"
// import { getGetStyle, initUno } from '/uno.config'
// export const getStyle = getGetStyle(cssText)


function FAQDisclosure() {
  return (
    <Disclosure>
      <Disclosure.Button className="disclosure py-2">
        FAQ
      </Disclosure.Button>
      <Disclosure.Panel className="text-gray-500">
        Yes!
      </Disclosure.Panel>
    </Disclosure>
  )
}

function OptionsIndex() {
  const [data, setData] = useState('')
  const [txHash, setTxHash] = useState(undefined)



  const onChange = (e) => {
    const valid = addOutput(4)
    const invalid = { a: 1, b: '2' }
    const duckTyped = Value.Cast(TInput, invalid)
    console.log({ valid, duckTyped })
    // console.log('will throw', addInput(invalid))

    setData(e.target.value)
  }
  const onClick = async () => {
    const instCCAtom = new AtomDuck(castAbleImperfectAtom)
    console.log('main thread', { instCCAtom })

    const resp = await sendToBackground({
      name: 'add-applog',
      body: {
        input: data,
      },
    })
    setTxHash(resp)
  }

  return (
    <div un-cloak="true">
      <h1>
        Options for tabz
      </h1>

      <input onChange={onChange} value={data} />

      <button
        onClick={onClick}
        className="ui-active:bg-violet-5 bg-blue-400 hover:bg-blue-500 text-sm text-white font-mono font-light py-2 px-4 rounded border-2 border-blue-200">
        Hash TX 2
      </button>

      <p>TX HASH: {txHash}</p>

      <FAQDisclosure />
    </div>
  )
}

export default OptionsIndex
