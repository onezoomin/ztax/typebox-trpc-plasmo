
import { Disclosure } from '@headlessui/react'
import { MdCollectionsBookmark as Bookmarks } from '@react-icons/all-files/md/MdCollectionsBookmark'
import WindowTabs from './WindowTabs'
export function BookmarksDisclosure() {

    return (
        <Disclosure>
            <Disclosure.Button
                className="p-2 w-a bg-transparent b-none "
            >
                <Bookmarks size={'2em'} />
            </Disclosure.Button>
            <Disclosure.Panel className="w-a h-screen text-gray-700 overflow-scroll flex flex-col flex-justify-start flex-items-start">
                <WindowTabs></WindowTabs>
            </Disclosure.Panel>
        </Disclosure>
    )
}