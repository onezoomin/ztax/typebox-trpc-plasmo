
// https://react-icons.github.io/react-icons/icons?name=md
import { MdHistory as HistoryIcon } from '@react-icons/all-files/md/MdHistory'
// import { MdHistory  } from 'react-icons/md'

import { useState } from "react";
import { sendToBackground } from "@plasmohq/messaging";

import WindowTabs, { WindowTabsDisclosure } from "~components/WindowTabs";
import { Disclosure } from '@headlessui/react'

export function History() {
    const [historyArray, setHistoryArray] = useState<chrome.history.HistoryItem[]>([]);
    (async () => {
        const hist = await sendToBackground({
            name: 'history',
            body: {
                params: {
                    text: '',
                    maxResults: 10
                },
            },
        })
        console.log(hist);

        setHistoryArray(hist)
    })()

    return (
        <>
            {historyArray.map(eachHistoryEntry => (
                <span key={eachHistoryEntry.id} className="even:bg-blue-200 w-sm odd:bg-teal-300">
                    {eachHistoryEntry.title}
                </span>
            ))}
        </>
    )
}
/* TODO get presetIcons working <div className="i-material-symbols:history-rounded"></div> */
export function HistoryDisclosure() {


    return (
        <Disclosure>
            <Disclosure.Button
                className="disclosure p-2 w-a bg-transparent b-none"
            >
                <HistoryIcon size={'2em'} />
            </Disclosure.Button>
            <Disclosure.Panel className="[&>*:nth-child(odd)]:bg-opacity-30 w-sm h-screen text-gray-700 overflow-scroll flex flex-col flex-justify-start flex-items-start">
                <History></History>
            </Disclosure.Panel>
        </Disclosure>
    )
}
