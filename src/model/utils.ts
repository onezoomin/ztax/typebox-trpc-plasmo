import type { TObject } from '@sinclair/typebox'
import { Type } from '@sinclair/typebox'
import { Value } from '@sinclair/typebox/value'
import keyBy from 'lodash-es/keyBy'

const { Readonly } = Type

export const warnOrThrow = (typeBoxType: TObject, testObj: any, msg = 'errors:', shouldThrow = false) => {
  const errors = [...Value.Errors(typeBoxType, testObj)]
  const mappedErrors = keyBy(errors, 'path')
  if (shouldThrow) {
    console.error(msg, { testObj, mappedErrors })
    throw new Error(msg)
  }
  else {
    console.warn(msg, { testObj, mappedErrors })
  }
}

export function CreateDuckClass<thisType>(typeBoxType: TObject, newClassName: string) {
  // type thisType = Static<typeof typeBoxType>
  // this fx defines an anonymous class with the CheckAndCast pattern and returns it
  const DuckClass = {
    [newClassName]: class /* no way to dynamically name here without eval */ {
      constructor(newObj: thisType) {
        if (Value.Check(typeBoxType, newObj)) {
          Object.assign(this, newObj)
        }
        else {
          warnOrThrow(typeBoxType, newObj, 'ducktyping with errors:')
          Object.assign(this, Value.Cast(typeBoxType, newObj))
        }
        // return this // implicit for constructor
      }
    },
  }
  return DuckClass[newClassName]
}

export function CreateStrictClass<thisType>(typeBoxType: TObject, newClassName: string) {
  // type thisType = Static<typeof typeBoxType>
  // this fx defines an anonymous class with the CheckAndCast strict pattern and returns it
  const StrictClass = {
    [newClassName]: class /*  no way to dynamically name here without eval */ {
      constructor(newObj: thisType) {
        if (Value.Check(typeBoxType, newObj))
          Object.assign(this, newObj)
        else
          warnOrThrow(typeBoxType, newObj, `${newClassName} constructor failed with errors:`, true)

        //   return this // implicit for constructor
      }
    },
  }
  return StrictClass[newClassName]
}

const classToCompiledCheckerMap = new Map() // to avoid multiple versions of the same class for the same schema floating around
export function CreateGuardedClass<thisType>(typeBoxType: TObject, newClassName: string, isStrict = true) {
  // type thisType = Static<typeof typeBoxType>
  const isKnownProp = (prop: string) => Object.keys(typeBoxType.properties).includes(prop)

  if (!classToCompiledCheckerMap.has(typeBoxType))
    // classToCompiledCheckerMap.set(typeBoxType, TypeCompiler.Compile(typeBoxType).Check) // uses code eval not allowed in browser ext
    classToCompiledCheckerMap.set(typeBoxType, Value.Check.bind(Value, typeBoxType))
  else
    console.warn('creating an additional GuardedClass for', typeBoxType)

  const checker = classToCompiledCheckerMap.get(typeBoxType)

  const proxyHandler = {
    get: (target, prop) => {
      const isKnown = isKnownProp(prop)
      console.log('getter in proxy guard', { isKnown, target, prop })
      if (!isKnown && isStrict)
        throw new Error(`${target.constructor.name} doesnt include ${prop}`)

      return target[prop]
    },
    set: (target, prop, value) => {
      const isKnown = isKnownProp(prop)

      console.log('setter in proxy guard', { isKnown, target, prop, value })
      if (!isKnown) {
        console.error(target, ` doesnt include ${prop}`)
        if (isStrict)
          throw new Error(`${target.constructor.name} doesnt include ${prop}`)
      }

      const testObj = { ...target, [prop]: value }
      if (!checker(testObj)) // if (!Value.Check(typeBoxType, testObj)) { // bound above with fixed arg: Value.Check.bind(Value, typeBoxType)
        warnOrThrow(typeBoxType, testObj, `${newClassName} assignment failed`, isStrict)

      // if the test object checks out do the assignment and return the value
      target[prop] = value
      return value
    }
  }

  // this fx defines an anonymous class with the CheckAndCast strict bodyguard pattern and returns it
  const GuardedClass = {
    [newClassName]: class /*  no way to dynamically name here without eval */ {
      constructor(newObj: thisType) {
        if (Value.Check(typeBoxType, newObj)) {
          Object.assign(this, newObj)
          return new Proxy(this, proxyHandler)
        }
        else {
          warnOrThrow(typeBoxType, newObj, `${newClassName} constructor failed`)
        }
      }
    },
  }
  return GuardedClass[newClassName]
}

export type Immutable<T> = {
  +readonly [P in keyof T]: T[P]
}

export function CreateImmutableClass<staticType>(typeBoxType: TObject, newClassName: string, strict = true) {
  const isKnownProp = (prop: string) => Object.keys(typeBoxType.properties).includes(String(prop))
  // type staticType = Static<typeof typeBoxType> // TODO see if this can work to remove usage boilerplate feeling

  type thisType = Immutable<staticType> // mark thisType as readonly so ts can catch attempted setters

  const proxyHandler: ProxyHandler<thisType> = {
    get: (target, prop) => {
      const isKnown = isKnownProp(String(prop))
      console.log('getter in immutable proxy', { isKnown, target, prop })
      if (!isKnown && strict)
        throw new Error(`${target.constructor.name} doesnt include ${String(prop)}`)

      return target[String(prop)]
    },
    set: (): never => {
      throw new Error('setting prop not permitted in immutable proxy')
    },
  }

  // returns a class with the CheckAndCast (maybe) strict bodyguard pattern
  const ImmutableClass = {
    [newClassName]: class /*  no way to dynamically name here without eval */ {
      constructor(newObj: thisType) {
        if (Value.Check(typeBoxType, newObj)) {
          Object.assign(this, newObj)
          return new Proxy(this as unknown as thisType, proxyHandler) as thisType
        }
        else {
          warnOrThrow(typeBoxType, newObj, `${newClassName} constructor failed`)
        }
      }
    },
  }
  return ImmutableClass[newClassName]
}
