import type { TSchema } from '@sinclair/typebox'
// import { TypeCompiler } from '@sinclair/typebox/compiler'
import { TRPCError } from '@trpc/server'
import { Type } from '@sinclair/typebox'
import { Value } from '@sinclair/typebox/value'

export const IoType = <T extends TSchema>(schema: T, references: TSchema[] = []) =>
  (value: unknown) => Value.Check(schema, value)
    ? value
    : (() => {
        const errors = Value.Errors(schema, references, value)
        // console.log(errors)
        const { path, message } = [...errors][0]
        throw new TRPCError({ message: `${message} for ${path}`, code: 'BAD_REQUEST' })
      })()

export const TInput = Type.Object({
  a: Type.Number(),
  b: Type.Number(),
})
export const addInput = IoType(TInput)

export const addOutput = IoType(Type.Number())
