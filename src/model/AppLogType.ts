import { Type as t } from '@sinclair/typebox'
import type { Static, TObject } from '@sinclair/typebox'
import { Value } from '@sinclair/typebox/value'
import keyBy from 'lodash-es/keyBy'
import { Mixin } from 'ts-mixer'
import { IoType } from './IoType'
import { agentID, type atomCID, attributeName, entityID, type hashOfatomCIDarray, operation, previousCID, utcTimeStampMS, value } from './AppLogPrimatives'
import type { Immutable } from './utils'
import { CreateDuckClass, CreateGuardedClass, CreateImmutableClass, CreateStrictClass } from './utils'

export const TAtom = t.Object({
  ag: agentID,
  ts: utcTimeStampMS,
  en: entityID,
  at: attributeName,
  vl: value,
  pv: previousCID,
  op: operation,
})
export type TAtom = Static<typeof TAtom>

export const castAbleImperfectAtom = {
  ag: 'agentID',
  ts: '123',
  en: 'entityID',
  at: 'attributeName',
  vl: 'input',
  pv: 'previousCID',
  ignoredOrPassed: 'extra',
  op: false,
}
export const perfectAtom = {
  ag: 'agentID',
  ts: 123,
  en: 'entityID',
  at: 'attributeName',
  vl: 'input',
  pv: 'previousCID',
  op: false,
}

export interface AtomDuck extends TAtom { } // implicitly includes all props from TAtom
export class AtomDuck extends CreateDuckClass<TAtom>(TAtom, 'AtomDuck') { }

export interface AtomStrict extends TAtom { }
export class AtomStrict extends CreateStrictClass<TAtom>(TAtom, 'AtomStrict') { }

export interface AtomGuard extends TAtom { }
export class AtomGuard extends CreateGuardedClass<TAtom>(TAtom, 'AtomGuard') { }

export interface AtomImmutable extends Immutable<TAtom> { }
export class AtomImmutable extends CreateImmutableClass<TAtom>(TAtom, 'AtomImmutable') { }

export interface Atom extends TAtom { }
export class Atom {
  // this is explicitly implementing a constructor with the CheckAndCast pattern
  constructor(newAtomObj: TAtom) {
    if (Value.Check(TAtom, newAtomObj)) {
      Object.assign(this, newAtomObj)
    //   return this
    }
    else {
      const errors = [...Value.Errors(TAtom, newAtomObj)]
      const mappedErrors = keyBy(errors, 'path')
      console.warn('ducktyping with errors:', mappedErrors)
      Object.assign(this, Value.Cast(TAtom, newAtomObj))
    //   return this
    }
  }
}

// this does not work as hoped
const AtomConstuctor = t.Constructor([TAtom], TAtom)
// const newAtomConst = new AtomConstuctor(castAbleImperfectAtom)

export function CheckAndCast<T>(newAtomObj: T, typeBoxType, theThis) {
  if (Value.Check(typeBoxType, newAtomObj)) {
    Object.assign(theThis, newAtomObj) // impure assignment onto theThis ref - antipattern
    //   return theThis
  }
  else {
    const errors = [...Value.Errors(typeBoxType, newAtomObj)]
    const mappedErrors = keyBy(errors, 'path')
    console.warn('ducktyping with errors:', mappedErrors)
    Object.assign(theThis, Value.Cast(typeBoxType, newAtomObj))
    //   return theThis
  }
}

export class GenericTypeBoxClass<T> {
  constructor(newObj: T, tbType: TObject) {
    type thisType = typeof this & T
    CheckAndCast<thisType>(newObj as thisType, tbType, this) // works but with impure function called from the constructor - antipattern
  }
}

export interface GenericAtom extends TAtom { }
export class GenericAtom extends GenericTypeBoxClass<TAtom> { }

export const atomToDagAtom = (atom: TAtom) => {
  const strictAtom = new AtomStrict(atom)
  return {
    ...strictAtom,
    cid: 'hashedAtom',
  }
}

export interface AtomUtilMethods extends TAtom { }
export class AtomUtilMethods {
  toDagAtom() {
    return atomToDagAtom(this)
  }
}
// main differences between an Atom and an AppLog are
// 1. an AppLog has a hash that uniquely identifies it
// 2. an AppLog can have a reference to the tx that it was part of
//    - this tx field can also later be signed by the agent which asserted the set of atoms
export class AppLogObj extends Mixin(AtomStrict, AtomUtilMethods) {
  ha: atomCID
  tx?: hashOfatomCIDarray

  async create(atom: TAtom, tx: string = undefined): Promise<AppLogObj> {
    if ((atom as any).tx)
      throw new Error('atoms should not contain txHash - it must be added afterwards')
    const dagAtom = this.toDagAtom()
    const { cid } = await encodeDagJson(dagAtom)
    VERBOSE('constructing AppLogObj', { atom, dagAtom, cid })
    return {
      ...atom,
      tx,
      ha: cid.toString(),
    }
  }
}
// export interface AppLogObj extends TAtom { } // implicitly includes all props

export const addAtomInput = IoType(TAtom)

export const addAtomOutput = IoType(t.Number())
