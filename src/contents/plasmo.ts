import type { PlasmoCSConfig } from "plasmo"

export const config: PlasmoCSConfig = {
    matches: ["https://www.plasmo.com/*"]
}

window.addEventListener("load", () => {
    console.log('loading plasmo content');

    document.body.style.background = "pink"
})