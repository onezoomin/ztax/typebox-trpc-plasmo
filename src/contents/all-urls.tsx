import type { PlasmoCSConfig, PlasmoGetOverlayAnchor } from "plasmo"

import cssText from "data-text:./style.css"

import { getGetStyle } from "~tailwind-utils"
import { BookmarksDisclosure } from "~components/Bookmarks";
import { HistoryDisclosure } from "~components/History";
import { WindowTabsDisclosure } from "~components/WindowTabs";

// TODO issues for non working icon situations
// import { MaterialSymbol } from 'react-material-symbols';
// import Face from 'react:@material-symbols/svg-400/outlined/face.svg'
// import HistoryTxt from 'data-text:@material-symbols/svg-400/rounded/history.svg'
// import History from 'react:@material-symbols/svg-400/rounded/history.svg'

export const config: PlasmoCSConfig = {
  matches: ["<all_urls>"],
  all_frames: false
}
export const getStyle = getGetStyle(cssText)

// const SIDEBAR_ID = 'onezoom-sidebar'
// export const getRootContainer = () => {
//   const sidebarNode = document.createElement('div')
//   sidebarNode.id = SIDEBAR_ID
//   document.body.appendChild(sidebarNode)
//   return sidebarNode
// }

// export const getOverlayAnchor: PlasmoGetOverlayAnchor = async () =>
//   document.querySelector("#onezoom-sidebar")

const OnezoomSidebar = () => {
  // initUno()
  // getStyle()
  return (
    <div
      className="bg-gray-100 fixed w-auto h-screen z-9999 flex flex-col"
    >
      <HistoryDisclosure />
      <WindowTabsDisclosure />
      <BookmarksDisclosure />
    </div>
  )
}

export default OnezoomSidebar