import { sendToBackground } from '@plasmohq/messaging'
import { Value } from '@sinclair/typebox/value'
import { useState } from 'react'
import { AtomDuck, castAbleImperfectAtom } from '~src/model/AppLogType'
import { TInput, addInput, addOutput } from '~src/model/IoType'
// import presetPrimitives from 'unocss-preset-primitives'
import presetUno from '@unocss/preset-uno'
import presetAttributify from '@unocss/preset-attributify'
import initUnocssRuntime from '@unocss/runtime'
import type { PlasmoGetStyle } from "plasmo"

export const getStyle: PlasmoGetStyle = () => {
  const style = document.createElement("style")
  style.textContent = `
    [un-cloak] {
      display: none;
    }
  `
  return style
}

function HistoryIndex() {
  const [data, setData] = useState('')
  const [txHash, setTxHash] = useState(undefined)

  initUnocssRuntime({
    defaults: {
      presets: [
        presetUno(),
        presetAttributify(),
        // presetPrimitives({
        //   prefix: 'ui',
        //   variants: 'enable',
        //   selector: 'data-bar-state',
        //   isAttrBoolean: false,
        // }),
      ],
    }
  })

  const onChange = (e) => {
    const valid = addOutput(4)
    const invalid = { a: 1, b: '2' }
    const duckTyped = Value.Cast(TInput, invalid)
    console.log({ valid, duckTyped })
    // console.log('will throw', addInput(invalid))

    setData(e.target.value)
  }
  const onClick = async () => {
    const instCCAtom = new AtomDuck(castAbleImperfectAtom)
    console.log('main thread', { instCCAtom })

    const resp = await sendToBackground({
      name: 'add-applog',
      body: {
        input: data,
      },
    })
    setTxHash(resp)
  }

  return (
    <div un-cloak>
      <h1>
        1 <a href="https://www.plasmo.com">Plasmo</a> Extension!
      </h1>
      <h2>This is the Option UI page!</h2>
      <input onChange={onChange} value={data} />

      <button
        onClick={onClick}
        className="ui-active:bg-violet-5 bg-blue-400 hover:bg-blue-500 text-sm text-white font-mono font-light py-2 px-4 rounded border-2 border-blue-200">
        Hash TX 1
      </button>

      <p>TX HASH: {txHash}</p>
    </div>
  )
}

export default HistoryIndex
