import { useState } from 'react'

import { sendToBackground } from '@plasmohq/messaging'
import { Type } from '@sinclair/typebox'
import { Value } from '@sinclair/typebox/value'

import "~style.css"

import { Disclosure } from '@headlessui/react'
// import History from 'react:@material-symbols/svg-400/rounded/history.svg'
import { MdHistory as History } from '@react-icons/all-files/md/MdHistory'

function MyDisclosure() {
  return (
    <Disclosure>
      <Disclosure.Button className="py-2">
        <History></History>
      </Disclosure.Button>
      <Disclosure.Panel className="text-gray-500">
        Yes! You can purchase a license that you can share with your entire
        team.
      </Disclosure.Panel>
    </Disclosure>
  )
}

function IndexPopup() {
  const [txHash, setTxHash] = useState(undefined)
  const [txInput, setTxInput] = useState(0)
  return (
    <div>
      <MyDisclosure />
      <input
        type="number"
        value={txInput}
        onChange={(e) => {
          console.log('isNumber?', Value.Check(Type.Number(), e.target.valueAsNumber))
          setTxInput(e.target.valueAsNumber)
        }}
      />

      <button
        onClick={async () => {
          const resp = await sendToBackground({
            name: 'hash-tx',
            body: {
              input: txInput,
            },
          })
          setTxHash(resp)
        }}>
        Hash TX 2
      </button>

      <p>TX HASH: {txHash}</p>
    </div>
  )
}

export default IndexPopup
