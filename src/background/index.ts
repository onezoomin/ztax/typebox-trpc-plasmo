import '@plasmohq/messaging/background'

import { initTRPC } from '@trpc/server'
import { addInput, addOutput } from '~src/model/IoType'
import { tabActiveChanged } from './tab-manager'
// import './tab-manager'

const t = initTRPC.create()

export const appRouter = t.router({
  add: t.procedure
    .input(addInput)
    .output(addOutput)
    .query(({ input }) => input.a + input.b), // type-safe
})

export const life = 47
console.log(`WELCOME - ${life}`, { appRouter })

chrome.tabs.onActivated.addListener(tabActiveChanged);

