import type { PlasmoMessaging } from '@plasmohq/messaging'

const HIDDEN_NUMBER = 541

const handler: PlasmoMessaging.MessageHandler = async (req, res) => {
  const { params = { text: '' } } = req.body
  console.log(params)
  let hist = await chrome.history.search(params)
  res.send(hist)
}

export default handler
