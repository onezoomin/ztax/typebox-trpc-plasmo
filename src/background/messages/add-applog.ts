import type { PlasmoMessaging } from '@plasmohq/messaging'
import { AtomImmutable } from '../../model/AppLogType'
import { Atom, AtomDuck, AtomGuard, AtomStrict, GenericAtom, TAtom, castAbleImperfectAtom, perfectAtom } from '~src/model/AppLogType'

const HIDDEN_NUMBER = 541

const appLogMessageHandler: PlasmoMessaging.MessageHandler = async (req, res) => {
  const { input } = req.body

  const atomDuckInstance = new AtomDuck(castAbleImperfectAtom)
  console.log({ atomDuckInstance })

  // typescript errors are expected in most cases:
  const nAtom = new Atom(castAbleImperfectAtom)
  console.log(input, nAtom)

  const gAtom = new GenericAtom(castAbleImperfectAtom, TAtom)
  console.log({ gAtom })

  try {
    const atomStrictInstance = new AtomStrict(castAbleImperfectAtom)
    console.log({ never: atomStrictInstance })
  }
  catch (error) {
    console.error(error)
  }

  const atomGuardedStrictInstance = new AtomGuard(perfectAtom)
  atomGuardedStrictInstance.vl = 'successfully set'
  console.log({ atomGuardedStrictInstance, vl: atomGuardedStrictInstance.vl })
  try {
    atomGuardedStrictInstance.vl = +2 // should throw when assigning a number where string is expected
  }
  catch (error) {
    console.error(error)
  }
  try {
    atomGuardedStrictInstance.nonExistingProp = +2 // should throw when assigning to unknown prop
  }
  catch (error) {
    console.error(error)
  }

  const atomImmutableInstance = new AtomImmutable(perfectAtom)
  console.log({ atomImmutableInstance })
  try {
    atomImmutableInstance.vl = 'doesnt matter' // should throw when assigning to an immutable instance
  }
  catch (error) {
    console.error(error)
  }

  res.send(input * HIDDEN_NUMBER)
}

export default appLogMessageHandler
