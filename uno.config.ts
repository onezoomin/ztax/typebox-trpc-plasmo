import { defineConfig, presetUno } from 'unocss'
// import presetPrimitives from 'unocss-preset-primitives'
import presetAttributify from '@unocss/preset-attributify'
import initUnocssRuntime from '@unocss/runtime'
// import presetIcons from '@unocss/preset-icons/dist/browser'
import type { PlasmoGetStyle } from "plasmo"
import rootCssText from "data-text:~style.css"
// import rms from 'data-text:react-material-symbols/dist/rounded.css'; // Place in your root app file. There are also `sharp.css` and `outlined.css` variants.

// import '@iconify-json/material-symbols'

const _presetIconsConfig = {
    collections: {
        'material-symbols': () => import('@iconify-json/material-symbols/icons.json').then(i => i.default),
        // carbon: () => import('@iconify-json/carbon/icons.json').then(i => i.default),
        // mdi: () => import('@iconify-json/mdi/icons.json').then(i => i.default),
        // logos: () => import('@iconify-json/logos/icons.json').then(i => i.default),
    }
}
export const defaultConfig = {
    presets: [
        presetUno(),
        presetAttributify(),
        // presetIcons(_presetIconsConfig),

        // presetPrimitives({
        //     prefix: 'ui',
        //     variants: 'enable',
        //     selector: 'data-bar-state',
        //     isAttrBoolean: false,
        // }),
    ],
    preflights: [
        {
            getCSS: ({ theme }) => `
            [un-cloak] { display: none; }
            ${'' && rootCssText}
          `
        }
    ]
}
export const initUno = () => {
    initUnocssRuntime({
        defaults: defaultConfig
    })
}
export const getGetStyle = (cssText = '') => {
    const retFx: PlasmoGetStyle = () => {
        // initUno()
        const customCss = cssText
        ///* rootCssText // moved to preflights */}
        const style = document.createElement("style")
        style.textContent = `  
      ${customCss}
    `
        console.log({ customCss, cssText })

        return style
    }
    console.log(retFx, cssText)
    return retFx
}

export default defineConfig(defaultConfig)